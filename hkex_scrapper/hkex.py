import io
import os
import time

import zipfile
import requests
import datetime


class HKEX:
    import io
    import os
    import time

    import zipfile
    import requests
    import datetime

    today = datetime.datetime.now()
    day_of_week = today.strftime('%w')

    datetime_for_url = 0
    if day_of_week == '0':
        datetime_for_url = today - datetime.timedelta(days=2)
    elif day_of_week == '6':
        datetime_for_url = today - datetime.timedelta(days=2)
    elif 0 < int(day_of_week) < 6:
        datetime_for_url = today

    date = datetime_for_url.strftime('%y%m%d')

    instruments_list = ['hsif', 'hsifa', 'hsitmc', 'hsitmca', 'hsio', 'hsioa', 'xhso', 'mhif', 'mhifa', 'mhitmc',
                        'mhitmca', 'mhio', 'mhioa', 'SECTIDXF', 'dividend', 'vhsf', 'hhif', 'hhifa', 'hhitmc',
                        'hhitmca',
                        'hhio', 'hhioa', 'xhho', 'mchf', 'mchfa', 'mchtmc', 'mcho', 'mxjf', 'mxjfa', 'chhf', 'bovf',
                        'mcxf', 'bsef', 'saff', 'hibor', 'stock', 'dqe', 'dqetmc', 'cusf', 'cusfa', 'custmc', 'CRMBCF',
                        'CRMBCFA', 'UCNF', 'UCNFA', 'CUSO', 'lmef', 'lmefa', 'GDUF',
                        'GDUFA', 'GDRF', 'GDRFA', 'IRONF', 'IRONFA']

    # date = '200529'

    def liquidity_adj_write_hsif_hhif_csv(hkex_csv_path, file_csv_path):
        import csv
        from dateutil.parser import parse

        with open(hkex_csv_path, newline='') as f:
            k = []
            rows = csv.reader(f)
            for row in rows:
                k.append(row)

        # a: after, d: daytime, c: combined
        # a_date, a_open, a_high, a_low, a_close, a_volume = l[10][1], l[16][1], l[16][2], l[16][3], l[16][4], l[16][5]
        # c_date, c_open, c_high, c_low, c_close, c_volume = l[10][6], l[16][6], l[16][7], l[16][8], l[16][10], l[16][9]
        # Assign value.
        d_date, d_open, d_high, d_low, d_close, d_volume = k[10][6], k[16][6], k[16][7], k[16][8], k[16][10], k[16][9]

        # For Expired contract OR the Settlement day
        if k[16][1] == 'EXPIRED' or int(k[17][9]) >= int(k[16][9]):
            d_date, d_open, d_high, d_low, d_close, d_volume = k[10][6], k[17][6], k[17][7], k[17][8], k[17][10], k[17][
                9]
        d_date = parse(d_date)

        new_row = [d_date, d_open, d_high, d_low, d_close, d_volume]

        with open(file_csv_path, 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(new_row)

        return

    def main():

        for i in range(len(instruments_list)):
            try:
                url = 'https://www.hkex.com.hk/eng/stat/dmstat/dayrpt/' + instruments_list[i] + date + '.zip'
                r = requests.get(url, allow_redirects=False)
                z = zipfile.ZipFile(io.BytesIO(r.content))
                z.extractall()
                # os.rename("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
                old_path = 'D:/Documents/PythonProject/Trading/RetrieveData/HkexDailyReport/' \
                           + instruments_list[i] + date + '.csv'
                new_path = 'D:/Documents/TradingData/PriceData/HKEX/HkexInstruments(daily)/' \
                           + instruments_list[i] + '/' + instruments_list[i] + date + '.csv'
                os.replace(old_path, new_path)
            except zipfile.BadZipFile:
                continue

        hsif_daily_path = 'D:/Documents/TradingData/PriceData/HKEX/HkexInstruments(daily)/hsif/' \
                          + 'hsif' + date + '.csv'
        hsif_corrected_path = 'D:/Documents/TradingData/PriceData/HKmarket/HSIF/HSIF_Interday_Study' \
                              '/HSIF_LiquidityCorrected(Non-adjusted).csv'
        liquidity_adj_write_hsif_hhif_csv(hkex_csv_path=hsif_daily_path, file_csv_path=hsif_corrected_path)
        print(f'Successfully write data to {hsif_corrected_path}. Date: {date}')
        hhif_daily_path = 'D:/Documents/TradingData/PriceData/HKEX/HkexInstruments(daily)/hhif/' \
                          + 'hhif' + date + '.csv'
        hhif_corrected_path = 'D:/Documents/TradingData/PriceData/HKmarket/HHIF/HHIF_Interday_Study' \
                              '/HHIF_LiquidityCorrected(Non-adjusted).csv'
        liquidity_adj_write_hsif_hhif_csv(hkex_csv_path=hhif_daily_path, file_csv_path=hhif_corrected_path)
        print(f'Successfully write data to {hhif_corrected_path}. Date: {date}')

    if __name__ == '__main__':
        main()
